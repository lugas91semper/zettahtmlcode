// Expected Result : 6
// Direction : Get the total of "1" in binary value of number input
const number = 221;

function result(num) {
  return num.toString(2).replace(/0/g,"").length
}

console.log(result(number));