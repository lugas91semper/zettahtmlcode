// Expected result : [7, 3, 1, 2, 5, 6, 9, 10, 4, 8]
// Direction : Mutate arr1 to return combined array with arr2. The conditions are :
// 1. odd number at beginning 
// 2. even number at the end of array 
// 3. Original arr1 at the middle

const arr1 = [1, 2, 5, 6, 9, 10];
const arr2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

function result(arr1, arr2) {
  const odd = arr2.filter((element)=>element%2 === 1);
  const even = arr2.filter((element)=>element%2 === 0);
  let oddReduceDuplice = odd.filter((element) => {
    return arr1.find(fd=>fd == element)?false:true;
  }).sort((a,b)=>{return b-a});
  let evenReduceDuplice = even.filter((element) => {
    return arr1.find(fd=>fd == element)?false:true;
  });
  return [...oddReduceDuplice, ...arr1, ...evenReduceDuplice]
}

console.log(result(arr1, arr2));