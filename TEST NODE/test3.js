// Expected Result : [false, true]
// Direction :
// The first value : If all of arr2 has bigger value than the biggest value of arr1;
// The second value : If some of arr2 has smaller value than the smallest value of arr1;
const arr1 = [4, 6, 2, 3, 5];
const arr2 = [1, 3, 4, 7, 9, 10];

function result(arr1, arr2) {
  // const res = [];
  const res1 = arr2.every(checkRes1);
  const res2 = arr2.find(fd=> fd < Math.min(...arr1))?true:false;
  return [res1, res2];
}

function checkRes1(data){
  return data > Math.max(...arr1);
}


console.log(result(arr1, arr2));